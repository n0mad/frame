#define VF_MATH_IMPLEMENTATION

#include <stdio.h>
#include <entry.h>
#include "editor.h"

bool create_application(application_t *out_app) {

	out_app->config.window_name = "Frame Playground";
	out_app->config.window_width = 640;
	out_app->config.window_height = 480;
	out_app->init = editor_init;
	out_app->update = editor_update;
	out_app->render = editor_render;
	out_app->on_resize = editor_on_resize;
	out_app->state = calloc(1, sizeof(editor_state_t));

	return true;
}

