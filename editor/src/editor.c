#include <core/vf_logger.h>
#include "editor.h"

bool editor_init(struct application *editor_instance) {
	print_log(LOG_INFO, "application editor successfully initialized");
	return true;
}

bool editor_update(struct application *editor_instance, float delta_time) {
	return true;
}

bool editor_render(struct application *editor_instance, float delta_time) {
	return true;
}

void editor_on_resize(struct application *editor_instance, int width, int height) {

}
