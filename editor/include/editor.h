#ifndef EDITOR_H
#define EDITOR_H

#include <core/vf_definitions.h>
#include <core/vf_application_type.h>

typedef struct {
	float delta_time;
	// include here things related to camera, but how?
} editor_state_t;

bool editor_init(struct application *editor_instance);

bool editor_update(struct application *editor_instance, float delta_time);

bool editor_render(struct application *editor_instance, float delta_time);

void editor_on_resize(struct application *editor_instance, int width, int height);


#endif // PROGRAM_H

