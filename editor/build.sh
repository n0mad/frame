#!/bin/sh
# Build script for engine
set echo on

mkdir -p ../bin

# Get a list of all the .c files.
c_files=$(find . -type f -name "*.c")

# echo "Files:" $cFilenames

assembly="editor"
compiler_flags="-g -fdeclspec -fPIC"
# -fms-extensions 
# -Wall -Werror
include_flags="-Iinclude/ -I/usr/local/include/ -I../engine/include/"
linker_flags="-L../bin/ -lengine -lm -Wl,-rpath,."

echo "Building $assembly..."
clang $c_files $compiler_flags -o ../bin/$assembly $include_flags $linker_flags

