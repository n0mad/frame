<h2 align="center">Frame</h2>
<p align="center">A custom OpenGL playground meant for FreeBSD</p><br>

<br><br>

### What is Frame

Frame is a project meant to create an OpenGL playground for BSD. That is, a graphics engine where to play around and test with graphics programming.

### Project status

The project is currently on a really early stage. Only the engine code has been separated from the editor, and it displays a plane with vertex colors in the window.

### How to build the project

The project requires a few dependencies to be installed:

```sh
$ pkg install glfw3
```

Once you have the dependencies installed, clone the repo and run the install script:

```sh
$ git clone https://codeberg.org/n0mad/frame.git

$ ./build-all.sh
```

### Acknowledgments

Some integrated resources in the project come from external repositories like:

* GLFW: https://www.glfw.org/
* Glad: https://glad.dav1d.de/

### License

This project is ISC licensed. Check the `LICENSE` file for more information.

### Afterword

Thanks for using it! If this was useful for you check out the Telegram channel where I post work in progress and graphics related content (it works from the web, no accounts required), star the repo and share it!
Also I'm running low on coffee, so if you want to see the project to speed up, consider donating (:

<a href="https://www.buymeacoffee.com/n0madcoder"><img src="https://img.shields.io/badge/donate-coffee-orange?&style=flat-square&logo=buy-me-a-coffee"></a>
<a href="https://patreon.com/n0madcoder"><img src="https://img.shields.io/badge/donate-patreon-yellow?&style=flat-square&logo=patreon"></a>
<a href="https://t.me/s/vertexfarm"><img src="https://img.shields.io/badge/channel-telegram-blue?&style=flat-square&logo=telegram"></a>

