#!/bin/sh
set echo on

mkdir -p ../bin

c_files=$(find . -type f -name "*.c")

assembly="engine"
compiler_flags="-g -shared -fdeclspec -fpic"
# -fms-extensions 
# -Wall -Werror
include_flags="-Iinclude/ -I/usr/local/include/"
linker_flags="-L/usr/local/lib/ -lglfw -lGL -lrt -ldl -lm -lpthread -lX11 -lxcb -lXau -lXdmcp"

echo "Building $assembly..."
clang $c_files $compiler_flags -o ../bin/lib$assembly.so $include_flags $linker_flags


# clang $(find engine/ -type f -name "*.c") -g -shared -fdeclspec -fpic -o ../bin/libengine.so -Iengine/include/ -I/usr/local/include/ -L/usr/local/lib/ -lglfw -lGL -lrt -ldl -lm -lpthread -lX11 -lxcb -lXau -lXdmcp
