#ifndef ENTRY_H
#define ENTRY_H

#define VF_MATH_IMPLEMENTATION

#include "core/vf_application_type.h"
#include "core/vf_engine.h"

/*
 * this is defined externally to create our program
 */
extern bool create_application(application_t *out_app);

/*
 * main entry point for the application
 */
int main (int argc, char **argv) {
	print_log(LOG_INFO, "launching main function");

	application_t editor_instance; // because this is the editor??

	// request the application instance
	if(!create_application(&editor_instance)){
		print_log(LOG_ERROR, "Couldn't find an app instance, or create the app");
		return 1;
	}
	
	// init the engine based on the editor instance??
	engine_init(&editor_instance);
	
	// error checking if the engine stops working
	if(!engine_loop()) {
		print_log(LOG_INFO, "app not close successfully");
		return 1;
	}

	// clean memory before closing
	engine_terminate();

	return 0;
}

#endif // ENTRY_H

