#ifndef VF_FILE_IO_H
#define VF_FILE_IO_H

#include <stdio.h>

void *load_file(const char *file_path);


#endif // VF_FILE_IO_H

