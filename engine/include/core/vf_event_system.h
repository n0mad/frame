#ifndef VF_EVENT_SYSTEM_H
#define VF_EVENT_SYSTEM_H

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include "vf_definitions.h"
#include "vf_logger.h"

// glfw: whenever the window size changes (by OS or by user resize) this cb function is executed
// void fb_size_callback(GLFWwindow *window, int width, int height);

// glfw: get mouse position in the current window
// void mouse_pos_callback(GLFWwindow *window, double x_pos, double y_pos);

// glfw: callback to handle errors
// void error_callback(int error, const char *error_description);

#endif // VF_EVENT_SYSTEM_H

