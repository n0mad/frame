#ifndef APPLICATION_H
#define APPLICATION_H

#include <string.h>
#include <stdio.h>
#include "platform/platform_glfw.h"
#include "core/vf_logger.h"
// #include "vf_camera.h" included on gl_context

// extern GLint program_id;
// extern GLuint VAO, VBO;

// this struct contains the program's data and status
/*typedef struct {
	vf_cam3d_t *main_cam;
	GLFWwindow *window;
} frame_instance_t;

extern frame_instance_t *client;*/

// this struct is used to avoid circular dependencies with vf_application_type defined one
struct application;

typedef struct {
	int window_height, window_width;
	const char *window_name;
} engine_config_t;

// creates main app
bool engine_init(struct application *engine_instance);

// description
bool engine_is_active();

// description
bool engine_loop();

// finish the program and clean used memory
void engine_terminate();


#endif // APPLICATION_H

