#ifndef VF_DLIST_H
#define VF_DLIST_H

typedef struct {
	unsigned int capacity;
	unsigned int length;
	unsigned int element_size;
	void* elements;
} vf_dlist_t;


extern void* create_dlist(unsigned int length, unsigned int element_size);

extern void destroy_dlist(void* dlist);


#endif // VF_DLIST_H

