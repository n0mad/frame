#ifndef LOGSYS_H
#define LOGSYS_H

#include <stdio.h>
#include <string.h>
#include <stdarg.h> //this is required to work with arguments
#include "vf_definitions.h"

#define LOG_MESSAGE_SIZE 128

/* log system enum.
 * Indicates which kind of info is
 * printed in the command line */
typedef enum {
    LOG_INFO = 0,
    LOG_WARNING,
    LOG_ERROR,
    LOG_DEBUG
} log_level_t;


/**
 * Custom console log to keep track of messages in the engine code
 * logType takes values from log_level_t
 * text is the actual message
 * ... expands arguments passed
 */
void print_log(int log_level_t, const char *message, ...);

/**
 * init the logging system storing logs to be saved into a file
 */
bool init_logging();

/**
 * terminate logging work, write data into log file
 */
void terminate_logging();

#endif /* LOGSYS_H */

