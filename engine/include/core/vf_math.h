#ifndef VF_MATH_H
#define VF_MATH_H

#include "core/vf_logger.h"
#include "core/vf_definitions.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef VFMAPI
#define VFMAPI extern
#endif

#ifndef PI 
#define PI 3.14159265358979323846f
#endif

#define DEG2RAD (PI/180.0f)
#define RAD2DEG (180.0f/PI)

/* Not working as expected exactly */
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(*a))

/* Return a float vector from a given matrix */
#define matrix_To_Float(matrix) (matrix_To_FloatV(matrix).v)

/* Return a float vector from a vec3 */
#define vec3_To_Float(vector) (vec3_To_FloatV(vector).v)

/* NOTE: Helper types to be used instead of array return types for *ToFloat functions */
typedef struct float3 { float v[3]; } float3;
typedef struct float16 { float v[16]; } float16;

/* Clamp a float value */
VFMAPI float clamp(float value, float max, float min);

/* Calculate the linear interpolation between two vectors */
VFMAPI float lerp(float start, float end, float amount);

/* return @degrees value as radians */
VFMAPI float deg_to_rad(float degrees);

/* return @radians value as degrees */
VFMAPI float rad_to_deg(float radians);

/* OpenGL style 4x4
 *
 *       | 0   4   8  12 |
 * mat = | 1   5   9  13 |
 *       | 2   6  10  14 |
 *       | 3   7  11  15 |
 */
typedef struct {
	float m[16];
} vf_mat4_t;

typedef struct {
    float x, y;
} vf_vec2_t;

typedef struct {
    float x, y, z;
} vf_vec3_t;

typedef struct {
    float x, y, z, w;
} vf_vec4_t;

/**
 * Vector Math
 */

/* vec2 with 0.0f value components */
VFMAPI vf_vec2_t vec2_zero(void);

/* vec2 with 1.0f value components */
VFMAPI vf_vec2_t vec2_one(void);

/* Add two Vectors (vec_a + vec_b) */
VFMAPI vf_vec2_t vec2_add(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Subtract two Vectors (vec_a - vec_b) */
VFMAPI vf_vec2_t vec2_subtract(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Calculate vec2 length */
VFMAPI float vec2_length(vf_vec2_t vec_a);

/* Calculate two vec2 dot product */
VFMAPI float vec2_dot_product(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Calculate distance between two vec2 */
VFMAPI float vec2_distance(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Calculate X-axis angle from two vec2 */
VFMAPI float vec2_angle(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* scale vec2 (multiplied by given value) */
VFMAPI vf_vec2_t vec2_scale(vf_vec2_t vec_a, float scale);

/* Multiply vec2 by vec2 */
VFMAPI vf_vec2_t vec2_multiply(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Invert vec2 */
VFMAPI vf_vec2_t vec2_invert(vf_vec2_t vec_a);

/* Divide vec2 by a float value */
VFMAPI vf_vec2_t vec2_float_div(vf_vec2_t vec_a, float value);

/* Divide vec2 by vec2 */
VFMAPI vf_vec2_t vec2_div(vf_vec2_t vec_a, vf_vec2_t vec_b);

/* Normalize given vec2 */
VFMAPI vf_vec2_t vec2_normalize(vf_vec2_t vec_a);

/* Calculate linear interpolation between two vec2 */
VFMAPI vf_vec2_t vec2_lerp(vf_vec2_t vec_a, vf_vec2_t vec_b, float amount);


/* vec3 with 0.0f components */
VFMAPI vf_vec3_t vec3_zero(void);

/* vec3 with 1.0f components */
VFMAPI vf_vec3_t vec3_one(void);

/* vec3 with 1.0f in X */
VFMAPI vf_vec3_t vec3_x(void);

/* vec3 with 1.0f in Y */
VFMAPI vf_vec3_t vec3_y(void);

/* vec3 with 1.0f in Z */
VFMAPI vf_vec3_t vec3_z(void);

/* create a vec3 from 3 float values */
VFMAPI vf_vec3_t vec3_from_float(float x, float y, float z);

/* Add two vec3 */
VFMAPI vf_vec3_t vec3_add(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Substract two vec3 */
VFMAPI vf_vec3_t vec3_subtract(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Multiply vec3 by scalar */
VFMAPI vf_vec3_t vec3_float_multiply(vf_vec3_t vec_a, float scalar);

/* Multiply vec3 by vec3 */
VFMAPI vf_vec3_t vec3_multiply(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Calculate two vec3 cross product */
VFMAPI vf_vec3_t vec3_cross_product(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Calculate a vec3 perpendicular vector */
VFMAPI vf_vec3_t vec3_perpendicular(vf_vec3_t vec_a);

/* Calculate vec3 length */
VFMAPI float vec3_length(vf_vec3_t vec_a);

/* returns the norm of vec_a, defined as the sum of the squares of its components */
VFMAPI float vec3_norm(vf_vec3_t vec_a);

/* description */
VFMAPI vf_vec3_t vec3_unit(vf_vec3_t vec_a);

/* Calculate two vec3 dot product */
VFMAPI float vec3_dot_product(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Calculate distance between two vec3 */
VFMAPI float vec3_distance(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* scale vec3 by provided scale value */
VFMAPI vf_vec3_t vec3_scale(vf_vec3_t vec_a, float scale);

/* Negate Vector 3 (Invert dir) */
VFMAPI vf_vec3_t vec3_invert(vf_vec3_t vec_a);

/* Divide vec3 by a float value */
VFMAPI vf_vec3_t vec3_float_div(vf_vec3_t vec_a, float value);

/* Divide vec3 by vec3 */
VFMAPI vf_vec3_t vec3_div(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Normalize vec3 */
VFMAPI vf_vec3_t vec3_normalize(vf_vec3_t vec_a);

/* Return min value for each component couple */
VFMAPI vf_vec3_t vec3_min(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Return max value for each component couple */
VFMAPI vf_vec3_t vec3_max(vf_vec3_t vec_a, vf_vec3_t vec_b);

/* Return vec3 as a float array */
VFMAPI float3 vec3_to_float_vector(vf_vec3_t vec_a);

/* Make vector normalized and orthogonal to another vector implementing Gram-Schmidt function */
VFMAPI void vec3_ortho_normalize(vf_vec3_t *vec_a, vf_vec3_t *vec_b);

/* Transform a vec3 by a given mat4 */
VFMAPI vf_vec3_t vec3_transform(vf_vec3_t vector, vf_mat4_t matrix);

/* Transform a vec3 by a quat rotation */
VFMAPI vf_vec3_t vec3_quat_rotate(vf_vec3_t vector, vf_vec4_t quat);

/* Calculate linear interpolation between two vec3 */
VFMAPI vf_vec3_t vec3_lerp(vf_vec3_t vec_a, vf_vec3_t vec_b, float amount);

/* Calculate reflected vec3 to a Normal(vec3) */
VFMAPI vf_vec3_t vec3_reflect(vf_vec3_t vector, vf_vec3_t normal);

/* Compute barycenter coordinates (u, v, w) for a point p relative to a triangle (a, b, c)
 * assuming p is on the triangle's plane */
VFMAPI vf_vec3_t vec3_barycenter(vf_vec3_t point, vf_vec3_t vec_a, vf_vec3_t vec_b, vf_vec3_t vec_c);


/* vec4 with 0.0f components */
VFMAPI vf_vec4_t vec4_zero(void);

/* vec4 with 1.0f components */
VFMAPI vf_vec4_t vec4_one(void);


/**
 * mat4 Math 
 */

/* Returns identity matrix */
VFMAPI vf_mat4_t mat4_identity();

/* Inverts given matrix */
VFMAPI vf_mat4_t mat4_invert(vf_mat4_t matrix);

/* Returns perspective projection matrix */
VFMAPI vf_mat4_t mat4_frustum(double left, double right, double bottom, double top, double near, double far);

/* Returns perspective projection matrix */
VFMAPI vf_mat4_t mat4_perspective(double fovy, double aspect, double near, double far);

/* Returns where the camera is looking */
VFMAPI vf_mat4_t mat4_look_at(vf_vec3_t origin, vf_vec3_t target, vf_vec3_t up);

/* Rotates a matrix the given @angle (radians) in the given @axis */
VFMAPI vf_mat4_t mat4_rotate(vf_vec3_t axis, float angle);

/* Multiplies Given @mat4_a times given @mat4_b */
VFMAPI vf_mat4_t mat4_multiply(vf_mat4_t mat4_a, vf_mat4_t mat4_b);

/* */
VFMAPI vf_mat4_t mat4_translate(vf_vec3_t translation);

/* */
VFMAPI vf_mat4_t mat4_scale(vf_vec3_t scale);


/**
 * Vector4 / quat Math
 */

/* Converts A quat rotation to a matrix rotation */
VFMAPI vf_mat4_t quat_to_mat4(vf_vec4_t quat);

/**
 * Returns normalized quat from
 * provided quat @quat
 */
VFMAPI vf_vec4_t quat_normalize(vf_vec4_t quat);

/**
 * Returns the rotation quat from
 * vec3 @axis and float @angle.
 *
 * angle value must be radians.
 */
VFMAPI vf_vec4_t quat_from_axis_angle(vf_vec3_t axis, float angle);

/**
 * quatConjugate -- calculates the inverse of a quat.
 * 		
 * https://mathworld.wolfram.com/quatConjugate.html 
 * 
 * Multiplying a quat by its inverse or conjugate gives us a real number
 *
 *
 * Parameters
 *  	quat -- desired quat to conjugate.
 *
 * Returns
 * 		inverted quat.
 * */
VFMAPI vf_vec4_t quat_conjugate(vf_vec4_t quat);

/* creates view matrix using quat as camera orientation */
VFMAPI vf_vec4_t quat_look_at(vf_vec3_t dir, vf_vec3_t up);

/* returns the product of second quat times first one
 * Order is important, that's why quat_b is named first. */
VFMAPI vf_vec4_t quat_multiply(vf_vec4_t quat_b, vf_vec4_t quat_a);


#ifdef __cplusplus
}
#endif

#endif /* VF_MATH_H */


// now we implement the functions in the header too
#ifdef VF_MATH_IMPLEMENTATION

#include <math.h> /* required for fminf() */

VFMAPI float deg_to_rad(float degrees) {
	return degrees * DEG2RAD;
}

VFMAPI float rad_to_deg(float radians) {
	return radians * RAD2DEG;
}

VFMAPI vf_vec2_t vec2_zero(void) {
    vf_vec2_t res = {0.0f, 0.0f};
    return res;
}

VFMAPI vf_vec2_t vec2_one(void) {
    vf_vec2_t res = {1.0f, 1.0f};
    return res;
}

VFMAPI vf_vec2_t vec2_add(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    vf_vec2_t res = {vec_a.x + vec_b.x, vec_a.y + vec_b.y};
    return res;
}

VFMAPI vf_vec2_t vec2_subtract(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    vf_vec2_t res = {vec_a.x - vec_b.x, vec_a.y - vec_b.y};
    return res;
}

VFMAPI float vec2_length(vf_vec2_t vec_a) {
    float res = sqrtf((vec_a.x * vec_a.x) + (vec_a.y * vec_a.y));
    return res;
}

VFMAPI float vec2_dot_product(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    float res = ((vec_a.x * vec_b.x) + (vec_a.y * vec_b.y));
    return res;
}

VFMAPI float vec2_distance(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    float res = sqrtf(((vec_a.x - vec_b.x) * (vec_a.x - vec_b.x)) + ((vec_a.y - vec_b.y) *\
				(vec_a.y - vec_b.y)));
    return res;
}

VFMAPI float vec2_angle(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    float res = atan2f((vec_b.y - vec_a.y), (vec_b.x - vec_a.x) * (180.0f/PI));

    if (res < 0)
        res += 360.0f;

    return res;
}

VFMAPI vf_vec2_t vec2_scale(vf_vec2_t vec_a, float scale) {
    vf_vec2_t res = {vec_a.x * scale, vec_a.y * scale};
    return res;
}

VFMAPI vf_vec2_t vec2_multiply(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    vf_vec2_t res = {(vec_a.x * vec_b.x),(vec_a.y * vec_b.y)};
    return res;
}

VFMAPI vf_vec2_t vec2_invert(vf_vec2_t vec_a) {
    vf_vec2_t res = {-vec_a.x, -vec_a.y};
    return res;
}

VFMAPI vf_vec2_t vec2_float_div(vf_vec2_t vec_a, float value) {
    vf_vec2_t res = {vec_a.x/value, vec_a.y/value};
    return res;
}

VFMAPI vf_vec2_t vec2_div(vf_vec2_t vec_a, vf_vec2_t vec_b) {
    vf_vec2_t res = {(vec_a.x/vec_b.x), (vec_a.y/vec_b.y)};
    return res;
}

VFMAPI vf_vec2_t vec2_normalize(vf_vec2_t vec_a) {
    vf_vec2_t res = vec2_float_div(vec_a, vec2_length(vec_a));
    return res;
}

VFMAPI vf_vec2_t vec2_lerp(vf_vec2_t vec_a, vf_vec2_t vec_b, float amount) {
    vf_vec2_t res = { 0 };

    res.x = vec_a.x + amount * (vec_b.x - vec_a.x);
    res.y = vec_a.y + amount * (vec_b.y - vec_a.y);

    return res;
}

VFMAPI vf_vec3_t vec3_zero(void) {
    vf_vec3_t res = {0.0f, 0.0f, 0.0f};
    return res;
}

VFMAPI vf_vec3_t vec3_one(void) {
    vf_vec3_t res = {1.0f, 1.0f, 1.0f};
    return res;
}

VFMAPI vf_vec3_t vec3_x(void) {
	vf_vec3_t res = {1.0f, 0.0f, 0.0f};
	return res;
}

VFMAPI vf_vec3_t vec3_y(void) {
	vf_vec3_t res = {0.0f, 1.0f, 0.0f};
	return res;
}

VFMAPI vf_vec3_t vec3_z(void) {
	vf_vec3_t res = {0.0f, 0.0f, 1.0f};
	return res;
}

VFMAPI vf_vec3_t vec3_from_float(float x, float y, float z) {
	vf_vec3_t res;

	res.x = x;
	res.y = y;
	res.z = z;

	return res;
}

VFMAPI vf_vec3_t vec3_add(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = {(vec_a.x + vec_b.x), (vec_a.y + vec_b.y), (vec_a.z + vec_b.z)};
    return res;
}

VFMAPI vf_vec3_t vec3_subtract(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = {(vec_a.x - vec_b.x), (vec_a.y - vec_b.y), (vec_a.z - vec_b.z)};
    return res;
}

VFMAPI vf_vec3_t vec3_float_multiply(vf_vec3_t vec_a, float scalar) {
    vf_vec3_t res = {vec_a.x * scalar, vec_a.y * scalar, vec_a.z * scalar};
    return res;
}

VFMAPI vf_vec3_t vec3_multiply(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = {(vec_a.x * vec_b.x), (vec_a.y * vec_b.y), (vec_a.z * vec_b.z)};
    return res;
}

VFMAPI vf_vec3_t vec3_cross_product(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = { 0 };

	res.x = (vec_a.y * vec_b.z) - (vec_a.z * vec_b.y);
	res.y = (vec_a.z * vec_b.x) - (vec_a.x * vec_b.z);
	res.z = (vec_a.x * vec_b.y) - (vec_a.y * vec_b.x);

	return res;
}

VFMAPI vf_vec3_t vec3_perpendicular(vf_vec3_t vec_a) {
    vf_vec3_t res = { 0 };

    float min = (float) fabs(vec_a.x);
    vf_vec3_t cardinal_axis = {1.0f, 0.0f, 0.0f};

    if (fabs(vec_a.y) < min)
    {
        min = (float) fabs(vec_a.y);
        vf_vec3_t new_cardinal_axis = {0.0f, 1.0f, 0.0f};
        cardinal_axis = new_cardinal_axis;
    }

    if (fabs(vec_a.z) < min)
    {
        min = (float) fabs(vec_a.z);
        vf_vec3_t new_cardinal_axis = {0.0f, 0.0f, 1.0f};
        cardinal_axis = new_cardinal_axis;
    }

    res = vec3_cross_product (vec_a, cardinal_axis);

    return res;
}

VFMAPI float vec3_length(vf_vec3_t vec_a) {
    float res = sqrtf((vec_a.x * vec_a.x) + (vec_a.y * vec_a.y) + (vec_a.z * vec_a.z));
    return res;
}

VFMAPI float vec3_norm(vf_vec3_t vec_a) {
    float res = (vec_a.x * vec_a.x) + (vec_a.y * vec_a.y) + (vec_a.z * vec_a.z);
    return res;
}

VFMAPI vf_vec3_t vec3_unit(vf_vec3_t vec_a) {
	vf_vec3_t res = vec3_zero();
	float vec_length = vec3_length(vec_a);
	
	if(vec_length != 0.0f) {
		res.x = res.x / vec_length;
		res.y = res.y / vec_length;
		res.z = res.z / vec_length;
	}

	return res;
}

VFMAPI float vec3_dot_product(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    float res = ((vec_a.x * vec_b.x) + (vec_a.y * vec_b.y) + (vec_a.z * vec_b.z));
    return res;
}

VFMAPI float vec3_distance(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    float x_dist = vec_b.x - vec_a.x;
    float y_dist = vec_b.y - vec_a.y;
    float z_dist = vec_b.z - vec_a.z;

    float res = sqrtf((x_dist * x_dist) + (y_dist * y_dist) + (z_dist * z_dist));
    return res;
}

VFMAPI vf_vec3_t vec3_scale(vf_vec3_t vec_a, float scale) {
    vf_vec3_t res = {(vec_a.x * scale), (vec_a.y * scale), (vec_a.z *scale)};
    return res;
}

VFMAPI vf_vec3_t vec3_invert(vf_vec3_t vec_a) {
    vf_vec3_t res = {-vec_a.x, -vec_a.y, -vec_a.z};
    return res;
}

VFMAPI vf_vec3_t vec3_float_div(vf_vec3_t vec_a, float value) {
    vf_vec3_t res = {(vec_a.x / value), (vec_a.y / value), (vec_a.z / value)};
    return res;
}

VFMAPI vf_vec3_t vec3_div(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = {(vec_a.x / vec_b.x), (vec_a.y / vec_b.y), (vec_a.z / vec_b.z)};
    return res;
}

VFMAPI vf_vec3_t vec3_normalize(vf_vec3_t vec_a) {
    vf_vec3_t res = vec_a;

    float length, i_length;

    length = vec3_length(vec_a);

    if(length == 0.0f)
        length = 1.0f;

    i_length = 1.0f/length;

    res.x *= i_length;
    res.y *= i_length;
    res.z *= i_length;

    return res;
}

VFMAPI vf_vec3_t vec3_min(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = { 0 };

    res.x = fminf(vec_a.x, vec_b.x);
    res.y = fminf(vec_a.y, vec_b.y);
    res.z = fminf(vec_a.z, vec_b.z);

    return res;
}

VFMAPI vf_vec3_t vec3_max(vf_vec3_t vec_a, vf_vec3_t vec_b) {
    vf_vec3_t res = { 0 };

    res.x = fmaxf(vec_a.x, vec_b.x);
    res.y = fmaxf(vec_a.y, vec_b.y);
    res.z = fmaxf(vec_a.z, vec_b.z);

    return res;
}

VFMAPI float3 vec3_to_float_vector(vf_vec3_t vec_a) {
    float3 buffer = { 0 };

    buffer.v[0] = vec_a.x;
    buffer.v[1] = vec_a.y;
    buffer.v[2] = vec_a.z;

    return buffer;
}

VFMAPI void vec3_ortho_normalize(vf_vec3_t *vec_a, vf_vec3_t *vec_b) {
    *vec_a = vec3_normalize(*vec_a);

    vf_vec3_t vres = vec3_cross_product(*vec_a, *vec_b);
	vres = vec3_normalize(vres);
    
	*vec_b = vec3_cross_product(vres, *vec_a);
}

VFMAPI vf_vec3_t vec3_transform(vf_vec3_t vector, vf_mat4_t matrix) {
    vf_vec3_t res = { 0 };
    
	float x, y, z;
    
	x = vector.x;
    y = vector.y;
    z = vector.z;

    res.x = (matrix.m[0] * x) + (matrix.m[4] * y) + (matrix.m[8] * z) + matrix.m[12];
    res.y = (matrix.m[1] * x) + (matrix.m[5] * y) + (matrix.m[9] * z) + matrix.m[13];
    res.z = (matrix.m[2] * x) + (matrix.m[6] * y) + (matrix.m[10] * z) + matrix.m[14];

    return res;
}

VFMAPI vf_vec3_t vec3_quat_rotate(vf_vec3_t vec, vf_vec4_t quat) {
    vf_vec3_t res = { 0 };

    res.x = vec.x * ((quat.x * quat.x) + (quat.w * quat.w) - (quat.y * quat.y) -\
			(quat.z * quat.z)) + vec.y * ((2 * quat.x * quat.y) - (2 * quat.x * quat.z)) + vec.z *\
				 ((2 * quat.x * quat.z) + (2 * quat.w * quat.y));
    res.y = vec.x * ((2 * quat.w * quat.z) + (2 * quat.x * quat.y)) + vec.y *\
			((quat.w * quat.w) - (quat.x * quat.x) + (quat.y * quat.y) - (quat.z * quat.z)) +\
				 vec.z * ((-2 * quat.w * quat.x) + (2 * quat.y * quat.z));
    res.z = vec.x * ((-2 * quat.w * quat.y) + (2 * quat.x * quat.z)) + vec.y *\
			((2 * quat.w * quat.x) + (2 * quat.y * quat.z)) + vec.z * ((quat.w * quat.w) -\
				(quat.x * quat.x) - (quat.y * quat.y) + (quat.z * quat.z));

    return res;
}

VFMAPI vf_vec3_t vec3_lerp (vf_vec3_t vec_a, vf_vec3_t vec_b, float amount) {
    vf_vec3_t res = { 0 };

    res.x = vec_a.x + (amount * (vec_b.x - vec_a.x));
    res.y = vec_a.y + (amount * (vec_b.y - vec_a.y));
    res.z = vec_a.z + (amount * (vec_b.z - vec_a.z));

    return res;
}

VFMAPI vf_vec3_t vec3_reflect (vf_vec3_t vector, vf_vec3_t normal) {
    /*  O is the original vector, N is the normal vector of the incident plane
        R = O - (2 * N * (DotProduct[ O, N]))*/

    vf_vec3_t res = { 0 };

    float dot_product = vec3_dot_product(vector, normal);

    res.x = vector.x - (2.0f * normal.x) * dot_product;
    res.y = vector.y - (2.0f * normal.y) * dot_product;
    res.z = vector.z - (2.0f * normal.z) * dot_product;

    return res;
}

VFMAPI vf_vec3_t vec3_barycenter (vf_vec3_t point, vf_vec3_t vec_a, vf_vec3_t vec_b, vf_vec3_t vec_c) {
    
	vf_vec3_t comp_vec_0, comp_vec_1, comp_vec_2;

    comp_vec_0 = vec3_subtract(vec_b, vec_a);
    comp_vec_1 = vec3_subtract(vec_c, vec_a);
    comp_vec_2 = vec3_subtract(point, vec_a);

    float dot_00 = vec3_dot_product(comp_vec_0, comp_vec_0);
    float dot_01 = vec3_dot_product(comp_vec_0, comp_vec_1);
    float dot_11 = vec3_dot_product(comp_vec_1, comp_vec_1);
    float dot_20 = vec3_dot_product(comp_vec_2, comp_vec_0);
    float dot_21 = vec3_dot_product(comp_vec_2, comp_vec_1);

    float demon = (dot_00 * dot_11) - (dot_01 * dot_01);

    vf_vec3_t res = { 0 };

    res.y = ((dot_11 * dot_20) - (dot_01 * dot_21)) / demon;
    res.z = ((dot_00 * dot_21) - (dot_01 * dot_20)) / demon;
    res.x = 1.0f - (res.z + res.y);

    return res;
}

VFMAPI vf_mat4_t mat4_identity() {
	vf_mat4_t res = {1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f};
	return res;
}

VFMAPI vf_vec4_t vec4_zero (void) {
    vf_vec4_t res = {0.0f, 0.0f, 0.0f, 0.0f};
    return res;
}

VFMAPI vf_vec4_t vec4_one (void) {
    vf_vec4_t res = {1.0f, 1.0f, 1.0f, 1.0f};
    return res;
}

/* Invert given matrix */
VFMAPI vf_mat4_t mat4_invert(vf_mat4_t matrix) {
	vf_mat4_t res = { 0 };

	float cache[16]; /* improve calc times can use a Mat here too... it's the same */
	float tmp[12];
	float inv_det;

	cache[0]  = matrix.m[0];
	cache[1]  = matrix.m[1];
	cache[2]  = matrix.m[2];
	cache[3]  = matrix.m[3];
	cache[4]  = matrix.m[4];
	cache[5]  = matrix.m[5];
	cache[6]  = matrix.m[6];
	cache[7]  = matrix.m[7];
	cache[8]  = matrix.m[8];
	cache[9]  = matrix.m[9];
	cache[10] = matrix.m[10];
	cache[11] = matrix.m[11];
	cache[12] = matrix.m[12];
	cache[13] = matrix.m[13];
	cache[14] = matrix.m[14];
	cache[15] = matrix.m[15];

	tmp[0]  = (cache[0] * cache[5]) - (cache[1] * cache[4]);
	tmp[1]  = (cache[0] * cache[6]) - (cache[2] * cache[4]);
	tmp[2]  = (cache[0] * cache[7]) - (cache[3] * cache[4]);
	tmp[3]  = (cache[1] * cache[6]) - (cache[2] * cache[5]);
	tmp[4]  = (cache[1] * cache[7]) - (cache[3] * cache[5]);
	tmp[5]  = (cache[2] * cache[7]) - (cache[3] * cache[6]);

	tmp[6]  = (cache[8] * cache[13]) - (cache[9] * cache[12]);
	tmp[7]  = (cache[8] * cache[14]) - (cache[10] * cache[12]);
	tmp[8]  = (cache[8] * cache[15]) - (cache[11] * cache[12]);
	tmp[9]  = (cache[9] * cache[14]) - (cache[10] * cache[13]);
	tmp[10] = (cache[9] * cache[15]) - (cache[11] * cache[13]);
	tmp[11] = (cache[10] * cache[15]) - (cache[11] * cache[14]);

	inv_det = 1.0f/((tmp[0] * tmp[11]) - (tmp[1] * tmp[10]) + (tmp[2] * tmp[9]) + (tmp[3] * tmp[8]) - (tmp[4] * tmp[7]) + (tmp[5] * tmp[6]));

	res.m[0]  = ( (cache[5] * tmp[11]) - (cache[6] * tmp[10]) + (cache[7] * tmp[9])) * inv_det;
	res.m[1]  = (-(cache[1] * tmp[11]) + (cache[2] * tmp[10]) - (cache[3] * tmp[9])) * inv_det;
	res.m[2]  = ( (cache[13] * tmp[5]) - (cache[14] * tmp[4]) + (cache[15] * tmp[3])) * inv_det;
	res.m[2]  = (-(cache[9]  * tmp[5]) + (cache[10] * tmp[4]) - (cache[11] * tmp[3])) * inv_det;

	res.m[4]  = (-(cache[4] * tmp[11]) + (cache[6]  * tmp[8]) - (cache[7]  * tmp[7])) * inv_det;
	res.m[5]  = ( (cache[0] * tmp[11]) - (cache[2]  * tmp[8]) + (cache[3]  * tmp[7])) * inv_det;
	res.m[6]  = (-(cache[12] * tmp[5]) + (cache[14] * tmp[2]) - (cache[15] * tmp[1])) * inv_det;
	res.m[7]  = ( (cache[8]  * tmp[5]) - (cache[10] * tmp[2]) - (cache[11] * tmp[1])) * inv_det;

	res.m[8]  = ( (cache[4] * tmp[10]) - (cache[5]  * tmp[8]) + (cache[7] * tmp[6])) * inv_det;
	res.m[9]  = (-(cache[0] * tmp[10]) + (cache[1]  * tmp[8]) - (cache[3] * tmp[6])) * inv_det;
	res.m[10] = ( (cache[12] * tmp[4]) - (cache[13] * tmp[2]) + (cache[15] * tmp[0])) * inv_det;
	res.m[11] = (-(cache[8]  * tmp[4]) + (cache[9]  * tmp[2]) - (cache[11] * tmp[0])) * inv_det;

	res.m[12] = (-(cache[4]  * tmp[9]) + (cache[5]  * tmp[7]) - (cache[6] * tmp[6])) * inv_det;
	res.m[13] = ( (cache[0]  * tmp[9]) - (cache[1]  * tmp[7]) + (cache[2] * tmp[6])) * inv_det;
	res.m[14] = (-(cache[12] * tmp[3]) + (cache[13] * tmp[1]) - (cache[14] * tmp[0])) * inv_det;
	res.m[15] = ( (cache[8]  * tmp[3]) - (cache[9]  * tmp[1]) + (cache[10] * tmp[0])) * inv_det;
	
	return res;
}


/* Return perspective projection matrix. Used in Vulkan project check this out for possible changes */
VFMAPI vf_mat4_t mat4_frustum(double left, double right, double bottom, double top, double near, double far) {
	vf_mat4_t res = { 0 };

	float right_left = (float)(right - left);
	float top_bottom = (float)(top - bottom);
	float delta_z = (float)(far - near);

	/* x */
	res.m[0] = ((float)near * 2.0f)/right_left;
	res.m[1] = 0.0f;
	res.m[2] = 0.0f;
	res.m[3] = 0.0f;

	/* y */
	res.m[4] = 0.0f;
	res.m[5] = ((float)near * 2.0f)/top_bottom;
	res.m[6] = 0.0f;
	res.m[7] = 0.0f;

	/* z */
	res.m[8] = 0.0f; //((float)left + (float)right)/rightLeft;
	res.m[9] = 0.0f; //((float)top + (float)bottom)/topBottom;
	res.m[10] = ((float)near + (float)far)/delta_z;
	res.m[11] = -1.0f; //0.0f;

	/* ? */
	res.m[12] = 0.0f;
	res.m[13] = 0.0f;
	res.m[14] = ((float)near * (float)far * 2.0f)/delta_z;
	res.m[15] = 0.0f;

	return res;
}


VFMAPI vf_mat4_t mat4_perspective(double fovy, double aspect, double near, double far) {
	
	vf_mat4_t res = { 0 };
	float f, fn;

	// we are taking half of the fovy to make trigonometry
	f  = 1.0f / tanf(fovy * 0.5f);
	fn = 1.0f / (near - far);
	
	// dividing our f by aspect maintains the aspect ratio of the model.
	res.m[0] = f / aspect; 
	res.m[1] = 0.0f;
	res.m[2] = 0.0f;
	res.m[3] = 0.0f;

	res.m[4] = 0.0f;
	// this is the  scaling factor of the Y axis in the projection matrix.
	res.m[5] = f;
	res.m[6] = 0.0f;
	res.m[7] = 0.0f;

	res.m[8] = 0.0f;
	res.m[9] = 0.0f;
	res.m[10] = (near + far) * fn;
	res.m[11] = -1.0f;

	res.m[12] = 0.0f;
	res.m[13] = 0.0f;
	res.m[14] = ((2.0f * near) * far) * fn;
	res.m[15] = 0.0f;

	// tricky one for Vulkan but we can solve this problem out. This can be solved in
	// the vertex shader too
	//res.m[5] *= -1.0f;

	return res;
}


/* Be careful on this one since we have been using it with Vulkan in 2020 and may have changes for OpenGL */
VFMAPI vf_mat4_t mat4_look_at(vf_vec3_t origin, vf_vec3_t target, vf_vec3_t up) {
	vf_mat4_t res = { 0 };
	
	//vec3 x, y, z;

	vf_vec3_t vfront, vside, vup;

	vfront = vec3_subtract(target, origin);
	vfront = vec3_normalize(vfront);

	vside = vec3_cross_product(vfront, up);
	vside = vec3_normalize(vside);

	vup = vec3_cross_product(vside, vfront);

	res.m[0] =  vside.x;
	res.m[1] =  vup.x;
	res.m[2] = -vfront.x;
	res.m[3] =  0.0f;

	res.m[4] =  vside.y;
	res.m[5] =  vup.y;
	res.m[6] = -vfront.y;
	res.m[7] =  0.0f;

	res.m[8] =   vside.z;
	res.m[9] =   vup.z;
	res.m[10] = -vfront.z;
	res.m[11] =  0.0f;

	res.m[12] = -vec3_dot_product(vside, origin);
	res.m[13] = -vec3_dot_product(vup, origin);
	res.m[14] = vec3_dot_product(vfront, origin);
	res.m[15] = 1.0f;
	
	/*x = vec3Subtract(target, origin);
	x = vec3Normalize(x);

	z = vec3CrossProduct(x, up);
	z = vec3Normalize(z);

	y = vec3CrossProduct(z, x);

	res.m[0] =  z.x;
	res.m[1] =  y.x;
	res.m[2] = -x.x;
	res.m[3] =  0.0f;

	res.m[4] =  z.y;
	res.m[5] =  y.y;
	res.m[6] = -x.y;
	res.m[7] =  0.0f;

	res.m[8] =   z.z;
	res.m[9] =   y.z;
	res.m[10] = -x.z;
	res.m[11] =  0.0f;

	res.m[12] = -vec3DotProduct(z, origin);
	res.m[13] = -vec3DotProduct(y, origin);
	res.m[14] = vec3DotProduct(x, origin);
	res.m[15] = 1.0f;*/

	return res;
}


VFMAPI vf_mat4_t mat4_rotate(vf_vec3_t axis, float angle) {
	vf_mat4_t res = { 0 };

	float x = axis.x;
	float y = axis.y;
	float z = axis.z;

	float sol_sin = sinf(angle);
	float sol_cos = cosf(angle);
	float i_sol_cos = 1.0f - sol_cos;

	res.m[0] = (x * x * i_sol_cos) + (sol_cos);
	res.m[1] = (y * x * i_sol_cos) + (z * sol_sin);
	res.m[2] = (z * x * i_sol_cos) - (y * sol_sin);
	res.m[3] = 0.0f;

	res.m[4] = (x * y * i_sol_cos) - (z * sol_sin);
	res.m[5] = (y * y * i_sol_cos) + (sol_cos);
	res.m[6] = (z * y * i_sol_cos) + (x * sol_sin);
	res.m[7] = 0.0f;

	res.m[8]  = (x * z * i_sol_cos) + (y * sol_sin);
	res.m[9]  = (y * z * i_sol_cos) - (x * sol_sin);
	res.m[10] = (z * z * i_sol_cos) + sol_cos;
	res.m[11] = 0.0f;

	res.m[12] = 0.0f;
	res.m[13] = 0.0f;
	res.m[14] = 0.0f;
	res.m[15] = 1.0f;

	return res;
}

VFMAPI vf_mat4_t mat4_multiply(vf_mat4_t mat4_a, vf_mat4_t mat4_b) {
	vf_mat4_t res = { 0 };

	res.m[0] = (mat4_a.m[0] * mat4_b.m[0]) + (mat4_a.m[1] * mat4_b.m[4]) + (mat4_a.m[2] * mat4_b.m[8]) + (mat4_a.m[3] * mat4_b.m[12]);
	res.m[1] = (mat4_a.m[0] * mat4_b.m[1]) + (mat4_a.m[1] * mat4_b.m[5]) + (mat4_a.m[2] * mat4_b.m[9]) + (mat4_a.m[3] * mat4_b.m[13]);
	res.m[2] = (mat4_a.m[0] * mat4_b.m[2]) + (mat4_a.m[1] * mat4_b.m[6]) + (mat4_a.m[2] * mat4_b.m[10]) + (mat4_a.m[3] * mat4_b.m[14]);
	res.m[3] = (mat4_a.m[0] * mat4_b.m[3]) + (mat4_a.m[1] * mat4_b.m[7]) + (mat4_a.m[2] * mat4_b.m[11]) + (mat4_a.m[3] * mat4_b.m[15]);

	res.m[4] = (mat4_a.m[4] * mat4_b.m[0]) + (mat4_a.m[5] * mat4_b.m[4]) + (mat4_a.m[6] * mat4_b.m[8]) + (mat4_a.m[7] * mat4_b.m[12]);
	res.m[5] = (mat4_a.m[4] * mat4_b.m[1]) + (mat4_a.m[5] * mat4_b.m[5]) + (mat4_a.m[6] * mat4_b.m[9]) + (mat4_a.m[7] * mat4_b.m[13]);
	res.m[6] = (mat4_a.m[4] * mat4_b.m[2]) + (mat4_a.m[5] * mat4_b.m[6]) + (mat4_a.m[6] * mat4_b.m[10]) + (mat4_a.m[7] * mat4_b.m[14]);
	res.m[7] = (mat4_a.m[4] * mat4_b.m[3]) + (mat4_a.m[5] * mat4_b.m[7]) + (mat4_a.m[6] * mat4_b.m[11]) + (mat4_a.m[7] * mat4_b.m[15]);
    
	res.m[8] = (mat4_a.m[8] * mat4_b.m[0]) + (mat4_a.m[9] * mat4_b.m[4]) + (mat4_a.m[10] * mat4_b.m[8]) + (mat4_a.m[11] * mat4_b.m[12]);
	res.m[9] = (mat4_a.m[8] * mat4_b.m[1]) + (mat4_a.m[9] * mat4_b.m[5]) + (mat4_a.m[10] * mat4_b.m[9]) + (mat4_a.m[11] * mat4_b.m[13]);
	res.m[10] = (mat4_a.m[8] * mat4_b.m[2]) + (mat4_a.m[9] * mat4_b.m[6]) + (mat4_a.m[10] * mat4_b.m[10]) + (mat4_a.m[11] * mat4_b.m[14]);
	res.m[11] = (mat4_a.m[8] * mat4_b.m[3]) + (mat4_a.m[9] * mat4_b.m[7]) + (mat4_a.m[10] * mat4_b.m[11]) + (mat4_a.m[11] * mat4_b.m[15]);
	
	res.m[12] = (mat4_a.m[12] * mat4_b.m[0]) + (mat4_a.m[13] * mat4_b.m[4]) + (mat4_a.m[14] * mat4_b.m[8]) + (mat4_a.m[15] * mat4_b.m[12]);
	res.m[13] = (mat4_a.m[12] * mat4_b.m[1]) + (mat4_a.m[13] * mat4_b.m[5]) + (mat4_a.m[14] * mat4_b.m[9]) + (mat4_a.m[15] * mat4_b.m[13]);
	res.m[14] = (mat4_a.m[12] * mat4_b.m[2]) + (mat4_a.m[13] * mat4_b.m[6]) + (mat4_a.m[14] * mat4_b.m[10]) + (mat4_a.m[15] * mat4_b.m[14]);
	res.m[15] = (mat4_a.m[12] * mat4_b.m[3]) + (mat4_a.m[13] * mat4_b.m[7]) + (mat4_a.m[14] * mat4_b.m[11]) + (mat4_a.m[15] * mat4_b.m[15]);
 
	return res;	
}

VFMAPI vf_mat4_t mat4_translate(vf_vec3_t translation) {
	vf_mat4_t res = mat4_identity();

	res.m[12] = translation.x;
	res.m[13] = translation.y;
	res.m[14] = translation.z;

	return res;
}

VFMAPI vf_mat4_t mat4_scale(vf_vec3_t scale) {
	vf_mat4_t res = mat4_identity();

	res.m[0] = scale.x;
	res.m[5] = scale.y;
	res.m[10] = scale.z;

	return res;
}


VFMAPI vf_mat4_t quat_to_mat4(vf_vec4_t quat) {
	vf_mat4_t res = { 0 };
	
	float xx = quat.x * quat.x;
	float xy = quat.x * quat.y;
	float xz = quat.x * quat.z;

	float yy = quat.y * quat.y;
	float yz = quat.y * quat.z;
	float zz = quat.z * quat.z;

	float wx = quat.w * quat.x;
	float wy = quat.w * quat.y;
	float wz = quat.w * quat.z;


	res.m[0] = 1 - 2 * (yy + zz);
	res.m[1] = 2 * (xy + wz);
	res.m[2] = 2 * (xz - wy);
	res.m[3] = 0.0f;

	res.m[4] = 2 * (xy - wz);
	res.m[5] = 1 - 2 * (xx + zz);
	res.m[6] = 2 * (yz + wx);
	res.m[7] = 0.0f;

	res.m[8]  = 2 * (xz + wy);
	res.m[9]  = 2 * (yz - wx);
	res.m[10] = 1 - 2 * (xx + yy);
	res.m[11] = 0.0f;

	res.m[12] = 0.0f;
	res.m[13] = 0.0f;
	res.m[14] = 0.0f;
	res.m[15] = 1.0f;

	return res;
}

VFMAPI vf_vec4_t quat_normalize(vf_vec4_t quat) {
	vf_vec4_t res = { 0 };

	return res;
}

VFMAPI vf_vec4_t quat_from_axis_angle(vf_vec3_t axis, float angle) {
	vf_vec4_t res = { 0.0f, 0.0f, 0.0f, 1.0f };

	return res;
}

VFMAPI vf_vec4_t quat_conjugate(vf_vec4_t quat) {
	vf_vec4_t res = { 0 };
	
	res.x = -quat.x;
	res.y = -quat.y;
	res.z = -quat.z;
	res.w = quat.w;

	return res;
}

VFMAPI vf_vec4_t quat_look_at(vf_vec3_t dir, vf_vec3_t up) {
	vf_vec4_t res = { 0 };

	return res;
}

VFMAPI vf_vec4_t quat_multiply(vf_vec4_t quat_b, vf_vec4_t quat_a) {
	vf_vec4_t res;

	res.x = quat_b.w * quat_a.x + quat_b.x * quat_a.w + quat_b.y * quat_a.z - quat_b.z * quat_a.y;
	res.y = quat_b.w * quat_a.y + quat_b.y * quat_a.w + quat_b.z * quat_a.x - quat_b.x * quat_a.z;
	res.z = quat_b.w * quat_a.z + quat_b.z * quat_a.w + quat_b.x * quat_a.y - quat_b.y * quat_a.x;
	res.w = quat_b.w * quat_a.w - quat_b.x * quat_a.x - quat_b.y * quat_a.y - quat_b.z * quat_a.z;

	return res;
}



#endif /* VF_MATH_IMPLEMENTATION */

