#ifndef APPLICATION_TYPE_H
#define APPLICATION_TYPE_H

#include "vf_engine.h"

typedef struct application {
	engine_config_t config;

	bool (*init)(struct application *app_instance);

	bool (*update)(struct application *app_instance, float delta_time);
	
	bool (*render)(struct application *app_instance, float delta_time);

	void (*on_resize)(struct application *app_instance, int width, int height);

	void* state;
} application_t;

#endif // APPLICATION_TYPE_H
