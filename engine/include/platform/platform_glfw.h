#ifndef GL_CONTEXT_H
#define GL_CONTEXT_H

#include "3rd_party/glad/glad.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
// TODO move the callbacks out of this file
// #include "core/vf_event_system.h"
#include "renderer/vf_camera.h" // included in vf_renderer? this makes a circular dep

// #define WIDTH 640
// #define HEIGHT 480

/* TODO this can be converted into a typedef struct somehow
 * and point functions outside
typedef struct {
	GLFWwindow *window;
	void (*gl_context_loop());
} gl_context_t; */

typedef struct {
	GLFWwindow *window;
	vf_vec2_t last_mouse_pos;
	bool first_mouse;
} vf_gl_context_t;

// inits a glfw window object and initializes its context
int glfw_window_context_init(const char *name, int width, int height);

// initialize 3d scene objects and values such as camera(s)
void gl_context_init();

// render loop for gl_context?
void gl_context_loop();

// return true if exit condition is not met
bool gl_context_is_active();

// clean memory for the gl_context
void gl_context_terminate();

// glfw: whenever the window size changes (by OS or by user resize) this cb function is executed
void fb_size_callback(GLFWwindow *window, int width, int height);

// glfw: get mouse position in the current window
void mouse_pos_callback(GLFWwindow *window, double x_pos, double y_pos);

// glfw: callback to handle errors
void error_callback(int error, const char *error_description);

#endif // GL_CONTEXT_H

