#ifndef VF_RENDERER_H
#define VF_RENDERER_H

#include "core/vf_definitions.h"
#include "vf_camera.h"

typedef struct {
	vf_cam3d_t *main_cam;
	float near_clip, far_clip, fov;
	vf_vec3_t target;
} vf_renderer_t;

// static should always go in the .c file, otherwise we are creating a new copy
// each time we include the header in the project
// static vf_renderer_t *renderer_ptr;

// description
bool renderer_init(void *state);

// description
void renderer_terminate(void *state);

#endif // VF_RENDERER_H

