#ifndef VF_ARCBALL_H
#define VF_ARCBALL_H

#include <stdlib.h>
#include <math.h>
#include "core/vf_math.h"


typedef enum {
	NO_AXES,
	CAM_AXES,
	BODY_AXES,
	OTHER_AXES,
	N_SETS
} vf_axis_set_t;

typedef float *constrain_set;

typedef struct {
	vf_vec3_t center;
	double radius;
	vf_vec4_t quat_now, quat_down, quat_drag;
	vf_mat4_t mat_now, mat_down;
	vf_vec3_t vec_now, vec_down, vec_from, vec_to, vec_r_from, vec_r_to;
	bool show_result, dragging;
	//constrain_set sets[N_SETS];
	//unsigned int set_sizes[N_SETS];
	//vf_axis_set_t axis_constraint;
	//unsigned int axis_index;
} vf_arcball_t;

extern vf_arcball_t *arcball;

/* description */
vf_arcball_t* create_arcball(void);

/* set center and size of the arcball controller */
void arcball_place(vf_arcball_t *arcball, vf_vec3_t center, double radius);

/* update mouse position */
void arcball_update_mouse(vf_arcball_t *arcball, vf_vec3_t current);

/* set a constraint to use (if desired) */
void arcball_set_constraint(vf_arcball_t *arcball, vf_axis_set_t constraint);

/* description */
void arcball_update(vf_arcball_t *arcball);

/* returns a rotation matrix defined by the controller in use */
vf_mat4_t arcball_value(vf_arcball_t *arcball);

/* begin drag instructions */
void arcball_begin_drag(vf_arcball_t *arcball);

/* description */
void arcball_end_drag(vf_arcball_t *arcball);

/* description */
void arcball_draw(vf_arcball_t *arcball);


// debug functions

/* description */
void arcball_show_result(vf_arcball_t *arcball);

/* description */
void arcball_hide_result(vf_arcball_t *arcball);


// internal functions

/* convert window coordinates to sphere coordinates */
vf_vec3_t mouse_on_sphere(vf_vec3_t mouse, vf_vec3_t ab_center, double ab_radius);

/* description */
vf_vec3_t constrain_to_axis(vf_vec3_t loose, vf_vec3_t axis);

/* description */
int nearest_constrain_axis(vf_vec3_t loose, vf_vec3_t *axis, int n_axes);

/* description */
vf_vec4_t quat_from_arcball_points(vf_vec3_t from, vf_vec3_t to);

/* descriprion */
void quat_to_arcball_points(vf_vec4_t quat, vf_vec3_t *arc_from, vf_vec3_t *arc_to);

/* description */
void draw_any_arc(vf_vec3_t from, vf_vec3_t to);

/* description */
void draw_half_arc(vf_vec3_t n);

/* description */
void arcball_draw_constraints(vf_arcball_t *arcball);

/* description */
void arcball_draw_drag_arc(vf_arcball_t *arcball);

/* description */
void arcball_draw_result_arc(vf_arcball_t *arcball);

#endif // VF_ARCBALL_H

