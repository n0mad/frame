#ifndef GLSL_LOADER_H
#define GLSL_LOADER_H

#include <stdio.h>
#include <stdlib.h>
#include "3rd_party/glad/glad.h"
#include "core/vf_logger.h"

/* reads plain text files with shaders and put its content into a memory buffer */
const char *shader_file_read(const char *file_path);

/* checks errors in compiled shaders. Could be changed for a bool to return something */
void check_shader(GLuint shader_id);

/* returns the program ID for the loaded shaders as GLuint */
GLuint load_shader(const char *glsl_vertex, const char *glsl_frag);

#endif // GLSL_LOADER_H

