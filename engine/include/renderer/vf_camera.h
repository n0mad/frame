#ifndef VF_CAMERA_H
#define VF_CAMERA_H

#include <stdlib.h>
#include "core/vf_math.h"

typedef struct {
	vf_vec3_t position;
	vf_vec3_t target;
	vf_vec3_t up;

	float fov;
	float near_clip;
	float far_clip;

	vf_mat4_t view;
	vf_mat4_t projection;

	/*struct {
		update_cam_perspective cpersp;
	} callbacks;*/
} vf_cam3d_t;

typedef void (*update_cam_perspective)(vf_cam3d_t *camera, unsigned int width, unsigned int height);

// LEGACY TODO reimplement in program instance struct
extern vf_cam3d_t *main_cam;

vf_cam3d_t* create_camera_3d(unsigned int width, unsigned int height);

void update_camera_perspective(vf_cam3d_t *camera, unsigned int width, unsigned int height);

void update_camera_3d_look_at(vf_cam3d_t *camera, vf_vec3_t target);

void clean_camera(vf_cam3d_t *camera);

#endif // VF_CAMERA_H

