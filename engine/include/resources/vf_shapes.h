#ifndef VF_SHAPES_H
#define VF_SHAPES_H

#include <stddef.h>
#include "3rd_party/glad/glad.h"
#include "core/vf_math.h"
#include "renderer/glsl_loader.h"
#include "core/vf_logger.h"

typedef struct {
	vf_vec3_t position;
	vf_vec3_t color;
} vf_vertex_t;

typedef struct {
	// a mesh type is going to have its own VAO, VBO, EBO
} vf_mesh_t;

extern GLuint vertex_buffer;
extern GLuint vertex_array;
extern GLuint element_buffer;
extern GLuint shader_program;

/* draw a quad with indexed vertices */
void test_quad_draw();

void draw_cube(int size);

/* draw a basic grid helper.
 * the spacing float can be used to define how much separation between lines we want
 */
void draw_grid(int divisions, float spacing);

void clean_shapes_stuff();

#endif // VF_SHAPES_H

