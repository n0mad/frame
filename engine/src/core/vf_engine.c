#include "core/vf_application_type.h"
#include "core/vf_engine.h"
#include "resources/vf_shapes.h"

// GLuint ref_program_id;
// GLuint VAO, VBO;

typedef struct {
	application_t *engine_instance;
	bool is_running, is_active;
	// int width, height;
	double last_time;
	bool is_initialized;
	void *renderer_system_state;
} engine_state_t;

static engine_state_t engine_state;

bool engine_init(application_t *out_app) {

	if(engine_state.is_initialized) {
		print_log(LOG_INFO, "program already initialized");
		return false;
	}

	// init frame instance
	// client = calloc(1, sizeof(frame_instance_t));
	engine_state.engine_instance = out_app;

	// init subsystems
	init_logging();

	// engine_state.first_mouse = true;
	engine_state.is_running = true;
	engine_state.is_active = true;
	
	// this is initializing our platform specific stuff. TODO cahnge with platform at some point
	glfw_window_context_init(engine_state.engine_instance->config.window_name,\
			engine_state.engine_instance->config.window_width,\
			engine_state.engine_instance->config.window_height);
	
	if(!engine_state.engine_instance->init(engine_state.engine_instance)) {
		print_log(LOG_ERROR, "failed to initalize program instance");
		return false;
	}

	// TODO order this. This should be called at renderer_init()
	// init_camera_3d();
	/*renderer->*/main_cam = create_camera_3d(engine_state.engine_instance->config.window_width,\
			engine_state.engine_instance->config.window_height);
	test_quad_draw();

	return true;
}

bool engine_is_active() {
	return gl_context_is_active();
}

bool engine_loop() {

	while(engine_state.is_running) {

		engine_state.is_running = engine_is_active();

		if(engine_state.is_active) {
			if(!engine_state.engine_instance->update(engine_state.engine_instance, 0.0f)) {
				print_log(LOG_ERROR, "app failed to update. closing down");
				engine_state.is_running = false;
				break;
			}
			
			if(!engine_state.engine_instance->render(engine_state.engine_instance, 0.0f)) {
				print_log(LOG_ERROR, "app failed to render. closing down");
				engine_state.is_running = false;
				break;
			}
		}
		// render here
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		vf_mat4_t model_matrix, model_view_matrix, full_transform_matrix;
		vf_vec3_t rotation_axis = { 0.0f, 0.0f, 1.0f };

		model_matrix = mat4_identity();	
		model_matrix = mat4_rotate(rotation_axis, deg_to_rad(30.0f));

		model_view_matrix = mat4_identity();
		model_view_matrix = mat4_multiply(model_matrix, main_cam->view); //client->main_cam->view);

		full_transform_matrix = mat4_multiply(model_view_matrix, main_cam->projection);
		
		glUseProgram(shader_program);
		
		// add uniform color, vertex array and such here
		
		GLint mvp_location = glGetUniformLocation(shader_program, "mvp");
		glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) &full_transform_matrix);
		
		glBindVertexArray(vertex_array);
		
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, NULL);
		
		// there is no need in detaching the vertex array every frame?
		//glBindVertexArray(0);
		
		glUseProgram(0);

		gl_context_loop();

	}

	return true;
	
}

void engine_terminate() {
	print_log(LOG_INFO, "Terminating application");
	clean_shapes_stuff();
	clean_camera(main_cam);
	gl_context_terminate();
}

