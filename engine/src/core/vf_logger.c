#include "core/vf_logger.h"

void print_log(int log_level_t, const char *message, ...) {
        
        va_list args;
        va_start(args, message);

        char tmp_buffer[LOG_MESSAGE_SIZE] = { 0 };

		const char *info_header[5] = { "INFO>    ", "WARNING> ", "ERROR>   ", "DEBUG>   "};
		strcpy(tmp_buffer, info_header[log_level_t]);
 
        //more complex: concatenate log type, message and a line break. Then print with args
        strcat(tmp_buffer, message);
        strcat(tmp_buffer, "\n");
        vprintf(tmp_buffer, args);

}

bool init_logging() {
	return true;
}

void terminate_logging() {

}

