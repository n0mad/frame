#include "resources/vf_shapes.h"

GLuint vertex_buffer, vertex_array, element_buffer, shader_program;

const vf_vertex_t vertices[] = {
	{ { 0.5f, 0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f } },
	{ { 0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f, 0.0f } },
	{ { -0.5f, -0.5f, 0.0f }, { 0.0f, 0.0f, 1.0f } },
	{ { -0.5f, 0.5f, 0.0f }, { 1.0f, 1.0f, 1.0f } }
};

unsigned short indices[] = {
	0, 1, 3,
	1, 2, 3,
};

void test_quad_draw() {
	
	print_log(LOG_INFO,"Starting ttd function");

	// create vertex shader & fragment shader
	// TODO these are currently being loaded locally, but they should be loaded from engine's
	// default shaders
	shader_program = load_shader("./shaders/default.vert", "./shaders/default.frag");
	
	print_log(LOG_INFO, "shader program attached %d", shader_program);
	
	// vertex array work & bind vertex array first
	// 1.
	glGenVertexArrays(1, &vertex_array);
	glBindVertexArray(vertex_array);

	print_log(LOG_INFO, "vertex array created");

	// vertex buffer work
	// 1. generate a vertex buffer and
	glGenBuffers(1, &vertex_buffer);

	// 2. tell opengl about that logical index is going to be attached to the GL_ARRAY_BUFFER
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

	// 3. tell opengl to store 
	// GL_STATIC_DRAW flag tells opengl to allocate it without 'taking care'
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// opengl element buffer object generation
	// 1.
	glGenBuffers(1, &element_buffer);
	
	// 2. tell opengl that the logical index for the element buffer object is going to be
	// attached to the GL_ELEMENT_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer);

	// 3. tell opengl to store
	// again GL_STATIC_DRAW means we don't care a lot about the mesh
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	print_log(LOG_INFO, "element buffer created");
	
	/* tell opengl that the first attribute of the array [0] is defined by 3 values 
	 * of type GL_FLOAT
	 */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vf_vertex_t), (char *)offsetof(vf_vertex_t, position));
	
	// specify which attribute are we going to work with. In this case is the first one
	glEnableVertexAttribArray(0);
	
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vf_vertex_t), (char *)offsetof(vf_vertex_t, color));
	glEnableVertexAttribArray(1);
	
	print_log(LOG_INFO, "vertex buffer created");
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	// Note, the Index buffer binding is stored in the Vertex Array Object.
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	// unbind vertex array buffer
	glBindVertexArray(0);

	// mandatory test on wireframe render :D
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	print_log(LOG_INFO, "reached end of ttd");
}

void draw_grid(int divisions, float spacing) {

}

void clean_shapes_stuff() {
	glDeleteBuffers(1, &vertex_buffer);
	glDeleteVertexArrays(1, &vertex_array);
	glDeleteProgram(shader_program);
}
