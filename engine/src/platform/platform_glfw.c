#include <stdio.h>
#include "platform/platform_glfw.h"

static vf_gl_context_t gl_ctx;

int glfw_window_context_init(const char *name, int width, int height) {
	
	print_log(LOG_INFO, "starting init glfw window context function");
	print_log(LOG_DEBUG, "received data is %s, %d, %d", name, width, height);

	glfwSetErrorCallback(error_callback);

	// close program if no glfw
	if(!glfwInit()) {
		print_log(LOG_ERROR, "cannot init glfw");
		return -1;
	}

	// define opengl version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	// extra glfw hints
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); 

	// context state init in here

	gl_ctx.window = glfwCreateWindow(width, height, name, NULL, NULL);

	// ensure window creation
	if(!gl_ctx.window){
		print_log(LOG_ERROR, "window is null");
		glfwTerminate();
		return -1;
	}

	// if we create the window, then make it part of the current context
	glfwMakeContextCurrent(gl_ctx.window);
	glfwSetInputMode(gl_ctx.window, GLFW_STICKY_KEYS, GL_TRUE);

	//glad stuff
	gladLoadGL();
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		print_log(LOG_ERROR, "failed to load GLAD");
    	return 1;
	}

	// required for X11
	glfwSwapInterval(1);

	// set desired GLFW callbacks
	glfwSetFramebufferSizeCallback(gl_ctx.window, fb_size_callback);
	glfwSetCursorPosCallback(gl_ctx.window, mouse_pos_callback);

	// the clear color unless required to change on the render loop is better to keep here
	glClearColor((float)0.7f, (float)0.7f, (float)0.7f, (float)1.0f);
	
	print_log(LOG_INFO, "window created!");
	print_log(LOG_INFO, "opengl version %s", glGetString(GL_VERSION));

	return 0;
}

void gl_context_loop() {
	// swap front and back buffers
	glfwSwapBuffers(gl_ctx.window);

	// process all pending events
	glfwPollEvents();
}

bool gl_context_is_active() {
	return glfwGetKey(gl_ctx.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(gl_ctx.window);
}

void gl_context_terminate() {
	glfwDestroyWindow(gl_ctx.window);
	glfwTerminate();
}

void error_callback(int error, const char *error_description) {
	print_log(LOG_ERROR, "GLFW error > %d, %s", error, error_description);
}

void fb_size_callback(GLFWwindow *window, int width, int height) {
	glViewport(0, 0, width, height);
	update_camera_perspective(main_cam, width, height);
}

void mouse_pos_callback(GLFWwindow *window, double x_pos, double y_pos) {
	if(gl_ctx.first_mouse) {
		gl_ctx.last_mouse_pos.x = x_pos;
		gl_ctx.last_mouse_pos.y = y_pos;
		gl_ctx.first_mouse = false;
	}

	float x_offset = gl_ctx.last_mouse_pos.x - x_pos;
	float y_offset = gl_ctx.last_mouse_pos.y - y_pos;

	gl_ctx.last_mouse_pos.x = x_pos;
	gl_ctx.last_mouse_pos.y = y_pos;

	// this only works while clicking + dragging
	if (glfwGetMouseButton(gl_ctx.window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
		printf("right mouse btn press at %.2f x, %.2f y\n", gl_ctx.last_mouse_pos.x, gl_ctx.last_mouse_pos.y);
	}
	if (glfwGetMouseButton(gl_ctx.window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
		printf("left mouse button pressed\n");
	}
}

