#include "renderer/vf_camera.h"

// introducing this in the renderer
vf_cam3d_t *main_cam;

vf_cam3d_t* create_camera_3d(unsigned int width, unsigned int height) {
	vf_cam3d_t *_cam;

	_cam = calloc(1, sizeof(vf_cam3d_t));

	_cam->position = (vf_vec3_t){2.0f, 2.0f, 2.0f};
	_cam->target = vec3_zero();
	_cam->up = vec3_z();
	_cam->fov = deg_to_rad(45.0f);
	_cam->near_clip = 0.01f;
	_cam->far_clip = 10.0f;
	_cam->view = mat4_identity();
	_cam->projection = mat4_identity();

	_cam->projection = mat4_perspective(_cam->fov, width/(float)height, \
			_cam->near_clip, _cam->far_clip);
	_cam->view = mat4_look_at(_cam->position, _cam->target, _cam->up);
	return (vf_cam3d_t*) _cam;
}

void update_camera_perspective(vf_cam3d_t *camera, unsigned int width, \
		unsigned int height) {
	camera->projection = mat4_perspective(camera->fov, width/(float)height, \
			camera->near_clip, camera->far_clip);
}

void update_camera_3d_look_at(vf_cam3d_t *camera, vf_vec3_t target) {
	camera->target = target;

	camera->view = mat4_look_at(camera->position, camera->target, camera->up);
}

void clean_camera(vf_cam3d_t *camera) {
	free(camera);
}

