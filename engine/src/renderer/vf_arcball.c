#include "renderer/vf_arcball.h"
#include <core/vf_math.h>

#define LG_NSEGS 4
#define NSEGS (1 << LG_NSEGS)

// vf_mat4_t arcball_identity_m = mat4_identity();

// float other_axis[][4] = {{-0.48, 0.80, 0.36, 1.0}};

vf_arcball_t *arcball;

// we need to change this into a pointer function that returns an arcball
vf_arcball_t* create_arcball(void) {
	vf_arcball_t *_ab;

	_ab = calloc(1, sizeof(vf_arcball_t));

	_ab->center = vec3_one();
	_ab->radius = 1.0f; // 0.75f;
	_ab->vec_down = _ab->vec_now = vec3_one();
	_ab->quat_down = _ab->quat_now = vec4_one();
	_ab->mat_down = _ab->mat_now = mat4_identity();
	_ab->show_result = _ab->dragging = false;
	//_ab->axis_constraint = NO_AXES;

	return (vf_arcball_t*) _ab;
}

void arcball_place(vf_arcball_t *arcball, vf_vec3_t center, double radius) {
	arcball->center = center;
	arcball->radius = radius;
}

void arcball_update_mouse(vf_arcball_t *arcball, vf_vec3_t current) {
	arcball->vec_now = current;
}

/*void arcball_set_constraint(vf_arcball_t *arcball, vf_axis_set_t constraint) {
	if(!arcball->dragging) arcball->axis_constraint = constraint;
}*/

void arcball_update(vf_arcball_t *arcball) {
	//int set_size = arcball->set_sizes[arcball->axis_constraint];
	//vf_vec3_t *set = (vf_vec3_t *)(arcball->sets[arcball->axis_constraint]);

	arcball->vec_from = mouse_on_sphere(arcball->vec_down, arcball->center, arcball->radius);
	arcball->vec_to = mouse_on_sphere(arcball->vec_now, arcball->center, arcball->radius);
	
	if(arcball->dragging) {
		/*if (arcball->axis_constraint != NO_AXES) {
			arcball->vec_from = constrain_to_axis(arcball->vec_from, set[arcball->axis_index]);
			arcball->vec_to = constrain_to_axis(arcball->vec_to, set[arcball->axis_index]);
		}*/

		arcball->quat_drag = quat_from_arcball_points(arcball->vec_from, arcball->vec_to);
		arcball->quat_now = quat_multiply(arcball->quat_drag, arcball->quat_down);
	} /*else {
		if (arcball->axis_constraint != NO_AXES) {
			arcball->axis_index = nearest_constrain_axis(arcball->vec_to, set, set_size);
		}

	}*/

	quat_to_arcball_points(arcball->quat_down, &arcball->vec_r_from, &arcball->vec_r_to);
	arcball->mat_now = quat_to_mat4(quat_conjugate(arcball->quat_now));
}

vf_mat4_t arcball_value(vf_arcball_t *arcball) {
	vf_mat4_t res;

	//arcball->mat_now = current;
	for(int i = 15; i >= 0; i-- ) {
		res.m[i] = arcball->mat_now.m[i];
	}

	return res;
}

void arcball_begin_drag(vf_arcball_t *arcball) {
	arcball->dragging = true;
	arcball->vec_down = arcball->vec_now;
}

void arcball_end_drag(vf_arcball_t *arcball) {
	arcball->dragging = false;
	arcball->quat_down = arcball->quat_now;

	// TODO is this the same as the for loop ? Investigate
	// arcball->mat_down = arcball->mat_now;
	
	for(int i = 15; i >= 0; i-- ) {
		arcball->mat_down.m[i] = arcball->mat_now.m[i];
	}
}

/*void arcball_draw(vf_arcball_t *arcball) {

}

void arcball_show_result(vf_arcball_t *arcball) {
	arcball->show_result = true;
}

void arcball_hide_result(vf_arcball_t *arcball) {
	arcball->show_result = false;
}*/

vf_vec3_t mouse_on_sphere(vf_vec3_t mouse, vf_vec3_t ab_center, double ab_radius) {
	vf_vec3_t arcball_mouse;
	double mag;

	arcball_mouse.x = (mouse.x - ab_center.x) / ab_radius;
	arcball_mouse.y = (mouse.y - ab_center.y) / ab_radius;

	mag = arcball_mouse.x * arcball_mouse.x + arcball_mouse.y * arcball_mouse.y;

	if(mag > 1.0) {
		double scale = 1.0 / sqrt(mag);
		arcball_mouse.x *= scale;
		arcball_mouse.y *= scale;
		arcball_mouse.z = 0.0f;
	} else {
		arcball_mouse.z = sqrt(1 - mag);
	}

	return arcball_mouse;
}

vf_vec3_t constrain_to_axis(vf_vec3_t loose, vf_vec3_t axis) {
	vf_vec3_t on_plane;
	float norm;
	on_plane = vec3_subtract(loose, vec3_scale(axis, vec3_dot_product(axis, loose)));
	norm = vec3_norm(on_plane);

	if(norm > 0.0f) {
		if (on_plane.z < 0.0f)
			on_plane = vec3_invert(on_plane);
		
		return vec3_scale(on_plane, 1/sqrt(norm));
	}

	if(axis.z == 1.0f) {
		on_plane = vec3_x();
	} else {
		on_plane = vec3_unit(vec3_from_float(-axis.y, axis.x, 0.0f));
	}

	return on_plane;
}

int nearest_constrain_axis(vf_vec3_t loose, vf_vec3_t *axis, int n_axes) {
	vf_vec3_t on_plane;
	float max = -1;
	float dot;
	int nearest = 0;

	for(int i = 0; i < n_axes; i++) {
		on_plane = constrain_to_axis(loose, axis[i]);
		dot = vec3_dot_product(on_plane, loose);

		if(dot > max) {
			max = dot;
			nearest = i;
		}
	}

	return nearest;
}

vf_vec4_t quat_from_arcball_points(vf_vec3_t from, vf_vec3_t to) {
	vf_vec4_t res;

	res.x = from.y * to.z - from.z * to.y;
	res.y = from.z * to.x - from.x * to.z;
	res.z = from.x * to.y - from.y * to.x;
	res.w = from.x * to.x + from.y * to.y + from.z * to.z;

	return res;
}

void quat_to_arcball_points(vf_vec4_t quat, vf_vec3_t *arc_from, vf_vec3_t *arc_to) {
	double s;
	s = sqrt(quat.x * quat.x + quat.y * quat.y);

	if(s == 0.0) {
		*arc_from = vec3_y();
	} else {
		*arc_from = vec3_from_float(-quat.y / s, quat.x / s, 0.0f);
	}

	arc_to->x = quat.w * arc_from->x - quat.z * arc_from->y;
	arc_to->y = quat.w * arc_from->y + quat.z * arc_from->x;
	arc_to->z = quat.x * arc_from->y - quat.y * arc_from->x;

	if(quat.w < 0.0f)
		*arc_from = vec3_from_float(-arc_from->x, -arc_from->y, 0.0f);
}

/*void draw_any_arc(vf_vec3_t from, vf_vec3_t to) {

}

void draw_half_arc(vf_vec3_t n) {

}

void arcball_draw_constraints(vf_arcball_t *arcball) {

}

void arcball_draw_drag_arc(vf_arcball_t *arcball) {

}

void arcball_draw_result_arc(vf_arcball_t *arcball) {

}*/

