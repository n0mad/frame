#include "renderer/glsl_loader.h"


const char *shader_file_read(const char *file_path) {

	printf("%s\n", file_path);
	
	char *buffer;

	FILE *file_pointer = fopen(file_path, "r");
	
	if(!file_pointer) {
		// deal with it
		print_log(LOG_ERROR, "Couldn't find anything at %s", file_path);
		exit(1);
	}

	// start reading at the end
	fseek(file_pointer, 0, SEEK_END);

	// get the file size
	long file_size = ftell(file_pointer);

	// set the file pointer at the start
	rewind(file_pointer);

	// allocate memory for our content. Use calloc since we know 
	// the size of the element
	// buffer = calloc(1, file_size +1);
	buffer = (char*) malloc(sizeof(char)*file_size);

	// read the file into the content char
	fread( buffer, file_size, 1, file_pointer);	

	// not so efficient but this way we export a const char*
	const char *export_buffer = buffer;

	fclose(file_pointer);

	return export_buffer;
}

void check_shader(GLuint shader_id) {
	int success;
	char info_log[512];
	
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(shader_id, 512, NULL, info_log);
		print_log(LOG_ERROR, "shader error:\n%s", info_log);
	}
	
}

GLuint load_shader(const char *glsl_vertex, const char *glsl_frag) {
	
    // read shaders
	const char *vs_source = shader_file_read(glsl_vertex);
	const char *vf_source = shader_file_read(glsl_frag);

    // compile vertex shader	
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	print_log(LOG_DEBUG, "Compiling vertex shader");
    glShaderSource(vertex_shader, 1, &vs_source, NULL);
    glCompileShader(vertex_shader);

	check_shader(vertex_shader);

    // compile fragment shader
	GLuint frag_shader = glCreateShader(GL_FRAGMENT_SHADER);

	print_log(LOG_DEBUG, "Compiling fragment shader");
    glShaderSource(frag_shader, 1, &vf_source, NULL);
    glCompileShader(frag_shader);

	check_shader(frag_shader);
    
	// link program
	GLuint program_id = glCreateProgram();

    glAttachShader(program_id, vertex_shader);
    glAttachShader(program_id, frag_shader);
    glLinkProgram(program_id);

    glDeleteShader(vertex_shader);
    glDeleteShader(frag_shader);

	free((void *)vs_source);
	free((void *)vf_source);

    return program_id;
}
