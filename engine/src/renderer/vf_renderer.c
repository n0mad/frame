#include "renderer/vf_renderer.h"

static vf_renderer_t *renderer_ptr;

bool renderer_init(void *state) {
	renderer_ptr = state;

	// init internal material shader
	// init UI shader if needed (check lib)
	// set projection matrix and such
	// initialize the camera position and angle
	renderer_ptr->main_cam = create_camera_3d(640, 480);

	// define near and far clip and scene initial color
	return true;
}

void renderer_terminate(void *state) {

}

